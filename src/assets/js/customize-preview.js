import $ from 'jquery';
import strip_tags from './helpers/strip-tags';

console.log('customizer');
//console.log(_themename);

wp.customize('_themename_site_info', (value) => {
    //console.log(value);
    value.bind((to)=>{
        console.log(to);
        $('.c-site-info__text').find('p:first').html(to);
    });
})

wp.customize('_themename_accent_color', (value) => {
    value.bind((to)=>{
        console.log(to);
        $('#_themename-bundle-inline-css').html(`
        a {
            color : ${to}
        }`
        );
    });
})