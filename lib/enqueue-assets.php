<?php
if(!function_exists('_themename_assets')) {
	function _themename_assets() {
		wp_enqueue_style( '_themename-bundle', get_template_directory_uri() . '/dist/assets/css/bundle.css', array(), true, 'all' );

		include(get_template_directory() .'/lib/inline-css.php');
		wp_add_inline_style('_themename-bundle', $inline_style);

		wp_enqueue_script( '_themename-scripts', get_template_directory_uri() . '/dist/assets/js/bundle.js', array('jquery'), true, true );
	}
}

if(!function_exists('_themename_admin_assets')) {
	function _themename_admin_assets() {
		wp_enqueue_style( '_themename-admin', get_template_directory_uri() . '/dist/assets/css/admin.css', array(), true, 'all' );
		wp_enqueue_script( '_themename-admin-scripts', get_template_directory_uri() . '/dist/assets/js/admin.js', array(), true, true );
	}
}

if(!function_exists('_themename_customize_preview_js')) {
	function _themename_customize_preview_js() {
		wp_enqueue_script( '_themename-customize-preview', get_template_directory_uri() . '/dist/assets/js/customize-preview.js', array('customize-preview', 'jquery'), '1.1.1', true );

		//wp_localize_script('_themename-customize-preview', '_themename', array( 'x' => 7));
	}
}

add_action('wp_enqueue_scripts', '_themename_assets');
add_action('admin_enqueue_scripts', '_themename_admin_assets');
add_action('customize_preview_init', '_themename_customize_preview_js');
