<?php

$accent_color = sanitize_hex_color(get_theme_mod('_themename_accent_color', '#20ddae'));

$inline_style  = "
	a {
		color : {$accent_color}
	}
";
