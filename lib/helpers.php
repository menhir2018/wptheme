<?php

if(!function_exists('_themename_post_meta')) {
	function _themename_post_meta() {
		printf(
			esc_html__( 'Posted on %s', '_themename' ),
			'<a href="' . esc_url( get_permalink() ) . '"><time datetime="' . esc_attr( get_the_date() ) . '">' . esc_html( get_the_date() ) . '</time></a>'
		);
		printf(
			esc_html__( 'By %s', '_themename' ),
			'<a href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a>'
		);

	}
}
if(!function_exists('_themename_readmore_link')) {
	function _themename_readmore_link() {
		echo ' <a class="c-post__readmore" href="' . esc_url( get_permalink() ) . '" title="' . the_title_attribute( [ 'echo' => false ] ) . '">';
		printf(
			wp_kses(
				__( 'Read More <span class="u-screen-reader-text">About %s</span>', '_themename' ),
				[
					'span' => [
						'class' => []
					]
				]
			),
			get_the_title()
		);
		echo '</a>';
	}
}
if(!function_exists('_themename_delete_post')) {
	function _themename_delete_post() {
		$url = add_query_arg([
			'action' => '_themename_delete_post',
			'post' => get_the_ID(),
			'nonce' => wp_create_nonce('_themename_delete_post_'. get_the_ID())
		], home_url());

		//var_dump(current_user_can('delete_post', get_the_ID() ));
		if(current_user_can('delete_post', get_the_ID() )){
			return "<a href='". esc_url($url)."'>".esc_html__('delete post', '_themename')."</a>";
		}
		
	}
}
