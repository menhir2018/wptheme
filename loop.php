<?php if(have_posts()) { ?>
	<?php while(have_posts()){ ?>
		<?php the_post(); ?>
        <?php get_template_part('template-parts/post/content', 'index') ?>

	<?php } ?>
	<?php
	the_posts_navigation();
	//do_action('_themename_after_pagination');
	?>
<?php } else { ?>
	<?php get_template_part('template-parts/post/content' ,'none') ?>
<?php } ?>
