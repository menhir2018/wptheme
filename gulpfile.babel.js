import gulp from 'gulp';
import yargs from 'yargs';
import sass from 'gulp-sass';
import cleanCss from 'gulp-clean-css';
import gulpif from 'gulp-if';
import sourcemaps from 'gulp-sourcemaps';
import imagemin from 'gulp-imagemin';
import del from 'del';
import webpack from 'webpack-stream';
import named from 'vinyl-named';
import zip from 'gulp-zip';
import replace from 'gulp-replace';
import info from './package.json';

const PRODUCTION = yargs.argv.prod;

const paths ={
    images: {
        src: 'src/assets/images/**/*.{jpg,jpeg,png,svg,gif}',
        dest: 'dist/assets/images'
    },
    scripts: {
        src: ['src/assets/js/bundle.js', 'src/assets/js/admin.js', 'src/assets/js/customize-preview.js'],
        dest: './dist/assets/js'
    },
    styles: {
        src: 'src/assets/scss/*.scss',
        dest: './dist/assets/css'
    },
    other: {
        src: ['src/assets/**/*', '!src/assets/{images,js,scss}', '!src/assets/{images,js,scss}/**/*'],
        dest: 'dist/assets/'
    },
    package: {
        src : ['**/*', '!node_modules{,/**}', '!packaged{,/**}', '!src{,/**}', '!.git{,/**}', '!.babelrc', '!gulpfile.babel.js', '!package.json', '!package-lock.json'],
        dest: 'packaged'
    },
    font: {
        src: ['node_modules/@fortawesome/fontawesome-free/{svgs,sprites,webfonts}/**/*'],
        dest: ['dist/assets/fontawesome']
    }
}

function defaultTask(cb) {
    // place code for your default task here
    console.log('hey');
    console.log(PRODUCTION);
    cb();
}

export const clean = () =>{
    return del(['dist']);
}
export const styles = (cb) =>{
    // body omitted
    console.log('we are in clean');
    return gulp.src(paths.styles.src)
        .pipe(gulpif(!PRODUCTION, sourcemaps.init()))
        .pipe(sass().on('error', sass.logError))
        .pipe(gulpif(PRODUCTION, cleanCss()))
        .pipe(gulpif(!PRODUCTION, sourcemaps.write()))
        .pipe(gulp.dest(paths.styles.dest));

}

export const watch = () =>{
    gulp.watch('src/assets/scss/**/*.scss', styles);
    gulp.watch('src/assets/js/**/*.js', scripts);
    gulp.watch(paths.images.src, images);
    gulp.watch(paths.other.src, copy);
}

export const images = () =>{
    return gulp.src(paths.images.src)
        .pipe(imagemin())
        .pipe(gulp.dest(paths.images.dest));
}

export const copyOthers = () =>{
    return gulp.src(paths.other.src)
        .pipe(gulp.dest(paths.other.dest));
}

export const copyFontawesome = () =>{
    return gulp.src(paths.font.src)
        .pipe(gulp.dest(paths.font.dest));
}

export const copy = gulp.series(copyOthers, copyFontawesome)


export const scripts = () =>{
    let mode = PRODUCTION ? 'production' : 'development';
    let devtool = PRODUCTION ? '' : 'inline-source-map';
    return gulp.src(paths.scripts.src)
        .pipe(named())
        .pipe(webpack({
            mode: mode,
            module: {
                rules: [
                    {
                        test: /\.js$/,
                        use: {
                            loader: 'babel-loader',
                            options: {
                                presets: ['@babel/preset-env'],

                            }
                        }
                    }
                ]
            },
            output: {
                filename: '[name].js'
            },
            externals: {
              jquery : 'jQuery'
            },
            devtool: devtool
        }))
        .pipe(gulp.dest(paths.scripts.dest));
}

export const compress =()=>{
    return gulp.src(paths.package.src)
        .pipe(replace('_themename', info.name))
        .pipe(zip(`${info.name}.zip`))
        .pipe(gulp.dest(paths.package.dest));
}

export const build = gulp.series(clean, gulp.parallel(styles, images, copy, scripts));
export const dev = gulp.series(clean, gulp.parallel(styles, images, copy, scripts), watch);
export const bundle = gulp.series(build, compress);

exports.default = defaultTask;
